//
//  ViewController.swift
//  CommunicationTest
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate  {

    var selectedpokemon : String = ""
    var hunger = 0
    var health = 100
    var counter = 1
    
    var hibernateselectedpokemon : String = ""
    var hibernateselectedpokemonhunger = 0
    var hibernateselectedpokemonhealth = 100
    var hibernateselectedpokemoncounter = 1

    @IBOutlet weak var wakeUpButton: UIButton!
    
    
    
    //timer
    
    var counterTimer = Timer()
    
    func startCounterTimer() {
        
        counterTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(decrementCounter), userInfo: nil, repeats: true)
        
        
    }
    @objc func decrementCounter(){
        counter += 1
        //print("hello")
        if(counter % 5 == 0){
            if (wakeUpButton.isHidden == true)
            {
                if (WCSession.default.isReachable) {
                    hunger += 10
                    if(hunger >= 100)
                    {
                        hunger = 100
                        if(health > 5)
                        {
                            health -= 5
                        }
                        else
                        {
                            health = 0
                        }
                        
                    }
                    else if(hunger >= 80)
                    {
                        if(health > 5)
                        {
                            health -= 5
                        }
                        else
                        {
                            health = 0
                        }
                        
                    }
                    
                    let message = ["pokemon": selectedpokemon,"hunger": hunger, "health" : health] as [String : Any]
                    WCSession.default.sendMessage(message, replyHandler: nil)
                    // output a debug message to the UI
                    outputLabel.insertText("\nUpdate Message sent to watch")
                    // output a debug message to the console
                    print("hunger: \(hunger), health: \(health)")
                }
                else {
                    print("update PHONE: Cannot reach watch")
                    outputLabel.insertText("\n update Cannot reach watch")
                }
            }
            else
            {
                let message = ["pokemon": hibernateselectedpokemon,"hunger": hibernateselectedpokemonhunger, "health" : hibernateselectedpokemonhealth] as [String : Any]
                WCSession.default.sendMessage(message, replyHandler: nil)
                // output a debug message to the UI
                outputLabel.insertText("\nUpdate Message sent to watch")
                // output a debug message to the console
                print("hunger: \(hunger), health: \(health)")
            
            }
            
        }
    }
   
    @IBOutlet weak var caterpieButton: UIButton!
    @IBOutlet weak var pikachuButton: UIButton!
    // MARK: Outlets
    @IBOutlet weak var outputLabel: UITextView!
    
    // MARK: Required WCSessionDelegate variables
    // ------------------------------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        //@TODO
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        //@TODO
    }
    
    // MARK: Receive messages from Watch
    // -----------------------------------
    
    // 3. This function is called when Phone receives message from Watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        // 1. When a message is received from the watch, output a message to the UI
        // NOTE: Since session() runs in background, you cannot directly update UI from the background thread.
        // Therefore, you need to wrap any UI updates inside a DispatchQueue for it to work properly.
        DispatchQueue.main.async {
            let messageBodyfeed = message["feed"] as! String
            let messageBodyhibernate = message["hibernate"] as! String
            if(messageBodyfeed == "feeding")
            {
                if(self.hunger >= 12)
                {
                    self.hunger -= 12
                }
                else{
                    self.hunger = 0
                }
                
            }
            else if(messageBodyhibernate == "true")
            {
                print("Phone: Hibernating mode selected ")
                //MARK: Hibernate code
                // cannot implement user defaults so proceeding
                
                self.hibernateselectedpokemon = "hibernate"
                self.hibernateselectedpokemonhunger = self.hunger
                self.hibernateselectedpokemonhealth = self.health
                self.hibernateselectedpokemoncounter = self.counter
                
                self.wakeUpButton.isHidden = false
                
                
            }
            self.outputLabel.insertText("\nMessage Received: \(message)")
        }
        
        // 2. Also, print a debug message to the phone console
        // To make the debug message appear, see Moodle instructions
        print("Received a message from the watch: \(message)")
    }

    
    // MARK: Default ViewController functions
    // -----------------------------------
    override func viewDidLoad() {
        
        super.viewDidLoad()
        wakeUpButton.isHidden = true
        
        // 1. Check if phone supports WCSessions
        print("view loaded")
        if WCSession.isSupported() {
            outputLabel.insertText("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            outputLabel.insertText("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            outputLabel.insertText("\nPhone does not support WCSession")
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: Actions
    // -------------------
    @IBAction func sendMessageButtonPressed(_ sender: Any) {
        
        // 2. When person presses button, send message to watch
        outputLabel.insertText("\nTrying to send message to watch")
        
        // 1. Try to send a message to the phone
        if (WCSession.default.isReachable) {
            let message = ["pokemon": selectedpokemon,"hunger": hunger, "health" : health] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
            startCounterTimer()
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
    }
    
    
    // MARK: Choose a Pokemon actions
    
    @IBAction func pokemonButtonPressed(_ sender: Any) {
        print("You pressed the pikachu button")
        selectedpokemon = "pikachu"
        caterpieButton.isHidden = true
    }
    @IBAction func caterpieButtonPressed(_ sender: Any) {
        print("You pressed the caterpie button")
        selectedpokemon = "caterpie"
        pikachuButton.isHidden = true
    }
    
    @IBAction func wakeUpButtonPressed(_ sender: Any) {
        wakeUpButton.isHidden = true
        
        //selectedpokemon = hibernateselectedpokemon
        hunger = hibernateselectedpokemonhunger
        health = hibernateselectedpokemonhealth
        counter = hibernateselectedpokemoncounter
        
        if (WCSession.default.isReachable) {
            let message = ["pokemon": selectedpokemon,"hunger": hunger, "health" : health] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler: nil)
            // output a debug message to the UI
            outputLabel.insertText("\nMessage sent to watch")
            // output a debug message to the console
            print("Message sent to watch")
            startCounterTimer()
        }
        else {
            print("PHONE: Cannot reach watch")
            outputLabel.insertText("\nCannot reach watch")
        }
        
    }
    
}

